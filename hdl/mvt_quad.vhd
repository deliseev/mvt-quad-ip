Library UNISIM;
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use UNISIM.vcomponents.all;


entity mvt_quad is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line
		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 5
	);
	port (
		-- Users to add ports here
        mvt_in_p: in std_logic_vector (3 downto 0);
        mvt_in_n: in std_logic_vector (3 downto 0);   

        clk_in : in std_logic;
        aresetn : in std_logic;

        
        clk_fast_00  : in std_logic; -- fast clock for deserializers
        clk_fast_180 : in std_logic; -- inverted  fast clock deserializers
        clk_div_00  : in std_logic; -- clk div (derived from fast clock)
        
        reset_main_counter : in std_logic; -- reset for a 32-bit counter (system clock)
               
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
	    s00_axi_rready	: in std_logic;
	    
	    -- Ports of Axi Stream Master S00_AXIS	
	    s00_axis_tready: in std_logic;
	    s00_axis_tdata: out std_logic_vector(31 downto 0);
	    s00_axis_tlast: out std_logic;
	    s00_axis_tvalid: out std_logic
		
	);
end mvt_quad;

architecture arch_imp of mvt_quad is

    component frontend_edge_finder is
    Port ( in_p : in STD_LOGIC;
           in_n : in STD_LOGIC;
           clk_fast_00 : in STD_LOGIC;
           clk_fast_180 : in STD_LOGIC;
           clk_div : in STD_LOGIC;
           system_clk : in STD_LOGIC;
           rst_in : in STD_LOGIC;
           polarity : in STD_LOGIC;
           sample_out : out STD_LOGIC_VECTOR (7 downto 0);
           posedge_index : out std_logic_vector(3 downto 0);
           posedge_index_valid : out std_logic;
           negedge_index : out std_logic_vector(3 downto 0);
           negedge_index_valid : out std_logic
           );
    end component frontend_edge_finder;
    
    component data_packager is
    Port ( main_counter : in STD_LOGIC_VECTOR (31 downto 0);
           rst_in : in STD_LOGIC;
           system_clk : in STD_LOGIC;
           data_package_32bit : out STD_LOGIC_VECTOR (31 downto 0);
           data_package_valid : out std_logic;
           data_package_last : out std_logic;
           mvt1_posedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt1_negedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt2_posedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt2_negedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt3_posedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt3_negedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt4_posedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt4_negedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           posedges_validity : in STD_LOGIC_VECTOR (3 downto 0);
           negedges_validity : in STD_LOGIC_VECTOR (3 downto 0)
           );
    end component data_packager;

	component mvt_quad_S00_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32; 
		C_S_AXI_ADDR_WIDTH	: integer	:= 5
		);
		port (
		reset_config       : out std_logic;
		reset_edge_counters  : out std_logic_vector(3 downto 0);
		reg_pos_neg_selector : out std_logic_vector(3 downto 0);
		test_input_selector  : out std_logic_vector(3 downto 0);
        sig_edge_counter0 : in std_logic_vector(31 downto 0);
        sig_edge_counter1 : in std_logic_vector(31 downto 0);
        sig_edge_counter2 : in std_logic_vector(31 downto 0); 
        sig_edge_counter3 : in std_logic_vector(31 downto 0); 
		
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component mvt_quad_S00_AXI;
        

    component mvt_axi_stream is
	generic (
		C_M_AXIS_TDATA_WIDTH	: integer	:= 32;
		C_M_START_COUNT	: integer	:= 32
	);
	port (
		-- Users to add ports here
		mvt_quad_data_package : in std_logic_vector (31 downto 0);
		mvt_quad_data_package_valid : in std_logic;
    	mvt_quad_data_package_last : in std_logic;
		-- User ports ends

		M_AXIS_ACLK	: in std_logic;
		M_AXIS_ARESETN	: in std_logic;
		M_AXIS_TVALID	: out std_logic;
		M_AXIS_TDATA	: out std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);
		M_AXIS_TSTRB	: out std_logic_vector((C_M_AXIS_TDATA_WIDTH/8)-1 downto 0);
		M_AXIS_TLAST	: out std_logic;
		M_AXIS_TREADY	: in std_logic
	);
    end component;
	
	signal mvt_out_p: std_logic_vector (3 downto 0);  
	signal mvt_out_n: std_logic_vector (3 downto 0);

    signal bounce_in_buf : std_logic_vector (3 downto 0);
	signal reg_pos_neg_selector  : std_logic_vector (3 downto 0);
	signal test_input_selector : std_logic_vector (3 downto 0);
	signal rst : std_logic;
    
	signal reset_config : std_logic;
	signal rst_edge_counters : std_logic_vector (3 downto 0);
	signal data_package_valid : std_logic;
	signal data_package_last : std_logic;
	
	signal data_package_32bit : std_logic_vector (31 downto 0);
	
	signal main_counter : std_logic_vector (31 downto 0);

    type t_bit4x32 is array (0 to 3) of std_logic_vector (31 downto 0);
    signal sig_edge_counter : t_bit4x32;    
    

	type t_bit4x8 is array (0 to 3) of std_logic_vector (7 downto 0);
    signal deser_out : t_bit4x8;
	
	type t_bit4x4 is array (0 to 3) of std_logic_vector (3 downto 0);
	signal p_index, n_index : t_bit4x4;
	
	signal p_indices_status, n_indices_status : std_logic_vector (3 downto 0);
	
	
begin

-- Instantiation of Axi Bus Interface S00_AXI
mvt_quad_S00_AXI_inst : mvt_quad_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
	    reset_config => reset_config,
    	reg_pos_neg_selector => reg_pos_neg_selector,
    	test_input_selector => test_input_selector,
        sig_edge_counter0 => sig_edge_counter(0),
        sig_edge_counter1 => sig_edge_counter(1),
        sig_edge_counter2 => sig_edge_counter(2),
        sig_edge_counter3 => sig_edge_counter(3),        
		S_AXI_ACLK	=> clk_in,
		S_AXI_ARESETN	=> aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready
	);
	
	axi_output: mvt_axi_stream
	generic map(
		C_M_AXIS_TDATA_WIDTH	=> 32,
		C_M_START_COUNT => 32
	)
	port map(
		-- Users to add ports here
		mvt_quad_data_package => data_package_32bit,
		mvt_quad_data_package_valid => data_package_valid,
		mvt_quad_data_package_last => data_package_last,

		-- Global ports
		M_AXIS_ACLK	=> clk_in,
		M_AXIS_ARESETN => aresetn,
		M_AXIS_TVALID => s00_axis_tvalid,	
		M_AXIS_TDATA =>	s00_axis_tdata,
		M_AXIS_TSTRB => open,	
		M_AXIS_TLAST => s00_axis_tlast,
		M_AXIS_TREADY => s00_axis_tready
	);

	-- Add user logic here
	rst <= reset_config or (not aresetn);
	
	
-- Vorläufig auskommentiert um BUFGCE außerhalb des IP-Cores herauszuziehen
--	-- BUFGCE clock didider: it provides clock signal which has the same frequency as the fabric. 
--	-- First of all this clock must be routed to the CLKDIV input of the deserializer.
--	-- It is also important to realize that the registered data at the output of the deserializer is aligned  
--	-- to this clock domain (clk_slow_00). 
--	-- More details can be found here: https://support.xilinx.com/s/article/67885
--    Buf_Divider: BUFGCE_DIV
--    generic map (
--         BUFGCE_DIVIDE => 4)
--    port map (
--         I   => clk_fast_00,
--         CE  => '1',
--         CLR => '0',
--         O   => clk_slow_00);
	
	Gen_Input: 
    for i in 0 to 3 generate
        MVT_INPUT_Edge_Founder : frontend_edge_finder 
        port map ( 
           in_p => mvt_in_p(i),
           in_n => mvt_in_n(i),
           clk_fast_00 => clk_fast_00,
           clk_fast_180 => clk_fast_180,
           clk_div => clk_div_00,
           system_clk => clk_in,
           rst_in => rst,
           polarity => reg_pos_neg_selector(i),
           sample_out =>  deser_out(i),
           posedge_index => p_index(i),
           posedge_index_valid => p_indices_status (i),
           negedge_index => n_index(i),
           negedge_index_valid => n_indices_status (i)
           );
    end generate;
    
    
-- Four counters to count edges at respective MVT-inputs (usefull for calibration)    
    Edge_Counters:
    for i in 0 to 3 generate
        process (clk_in, rst, rst_edge_counters, p_indices_status)
        begin
            if (clk_in'event and clk_in='1') then 
                if (rst = '1' or rst_edge_counters(i)='1') then
                    sig_edge_counter(i) <= (others => '0');        
                else            
                    if (p_indices_status(i)='1') then
                        sig_edge_counter(i) <= std_logic_vector(unsigned(sig_edge_counter(i)) + 1);
                    end if;                     
                end if;
            end if;
        end process;
    end generate;
    
    MVT_Data_Packager : data_packager 
    port map ( 
        main_counter => main_counter,
        rst_in => rst,
        system_clk => clk_in,
        data_package_32bit => data_package_32bit,
        data_package_valid => data_package_valid,
        data_package_last => data_package_last,
        mvt1_posedge_index  => p_index(0),
        mvt1_negedge_index => n_index(0),
        mvt2_posedge_index => p_index(1),
        mvt2_negedge_index => n_index(1),
        mvt3_posedge_index => p_index(2),
        mvt3_negedge_index => n_index(2),
        mvt4_posedge_index => p_index(3),
        mvt4_negedge_index => n_index(3),
        posedges_validity => p_indices_status,
        negedges_validity => n_indices_status 
       );
    
    main_counter_routine : process (clk_in, reset_main_counter, aresetn)
    begin
        if (clk_in'event and clk_in='1') then 
            if (reset_main_counter = '1' or aresetn = '0') then
                main_counter <= (others => '0');
            else            
                main_counter <= std_logic_vector(unsigned(main_counter) + 1);
            end if;
        end if;
    end process;

end arch_imp;
