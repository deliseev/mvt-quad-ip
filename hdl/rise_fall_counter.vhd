----------------------------------------------------------------------------------
-- Company: SmartFPGA
-- Engineer: D. Eliseev
-- 
-- Create Date: 01/29/2023 03:56:19 PM
-- Design Name: 
-- Module Name: rise_fall_counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Two 16-bit counters. First one starts on sync_in and counts until 
--              the mvt_in signal gets high. The second counts during mvt_in is high.
-- 
---------------------------------------------------------------------------------- 


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using  
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rise_fall_counter is
    Port ( 
           rst_in: in STD_LOGIC;
           clk_in : in STD_LOGIC;
           sync_in : in STD_LOGIC;
           mvt_in : in std_logic;
           re_cnt : out STD_LOGIC_VECTOR (15 downto 0);
           fe_cnt : out STD_LOGIC_VECTOR (15 downto 0)
           );
end rise_fall_counter;

architecture Behavioral of rise_fall_counter is


signal counter : unsigned (15 downto 0);

signal rising_edge_counts_catch : unsigned (15 downto 0);
signal falling_edge_counts_catch : unsigned (15 downto 0);
signal rising_edge_caught : std_logic;

signal re_cnt_reg : unsigned (15 downto 0);
signal fe_cnt_reg : unsigned (15 downto 0);

signal sync_in_buf : std_logic_vector (1 downto 0);  -- buffers to store the signal 
signal mvt_in_buf : std_logic_vector (1 downto 0);   -- waveforms for two clk tacts


begin

    re_cnt <= std_logic_vector(re_cnt_reg);
    fe_cnt <= std_logic_vector(fe_cnt_reg);
    
    sync_signal_buf : process (clk_in, sync_in)
    begin
        if (clk_in'event and clk_in='1') then 
            sync_in_buf (1) <= sync_in_buf (0);
            sync_in_buf (0) <= sync_in;
        end if;
    end process;

    mvt_signal_buf : process (clk_in, mvt_in)
    begin
        if (clk_in'event and clk_in='1') then 
            mvt_in_buf (1) <= mvt_in_buf (0);
            mvt_in_buf (0) <= mvt_in;
        end if;
    end process;
    
    Counter_proc : process (clk_in)
    begin
        if (clk_in'event and clk_in='1') then

                if (sync_in_buf = "01") then
                    counter <= (others =>'0'); 
                else
                    counter <= counter+1;
                end if;
        end if;
    end process;
    
    
    push_edge_counts : process (clk_in)
    begin
        if (clk_in'event and clk_in='1') then
            if (sync_in_buf = "01") then
                rising_edge_counts_catch <= (others =>'0'); 
                rising_edge_caught <= '0';
                falling_edge_counts_catch <= (others =>'0');
            elsif (mvt_in_buf = "01" and rising_edge_caught ='0') then -- after the sync_in signal only the first rising edge of the mvt input will be stored
                rising_edge_counts_catch <= counter;
                rising_edge_caught <= '1';
            elsif (mvt_in_buf = "10") then
                falling_edge_counts_catch <= counter;
            end if;
        end if;
    end process;

    counters_to_output : process (clk_in)
    begin
        if (clk_in'event and clk_in='1') then
            if (rst_in ='1') then
                re_cnt_reg <= (others => '0');
                fe_cnt_reg <= (others => '0');
            elsif (sync_in_buf="01") then -- bring counters from the previous frame to the output
                re_cnt_reg<=rising_edge_counts_catch;
                fe_cnt_reg<=falling_edge_counts_catch;
            end if;
        end if;
    end process;
end Behavioral;
