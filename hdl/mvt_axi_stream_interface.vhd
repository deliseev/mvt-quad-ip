----------------------------------------------------------------------------------
-- Company: RWTH Aachen
-- Engineer: Dmitry Eliseev
-- 
-- Create Date: 01/30/2023 08:36:44 PM
-- Design Name: 
-- Module Name: mvt_axi_stream_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


entity mvt_axi_stream is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line

		-- Width of S_AXIS address bus. The slave accepts the read and write addresses of width C_M_AXIS_TDATA_WIDTH.
		C_M_AXIS_TDATA_WIDTH	: integer	:= 32;
		-- Start count is the number of clock cycles the master will wait before initiating/issuing any transaction.
		C_M_START_COUNT	: integer	:= 10
	);
	port (
		-- Users to add ports here
		mvt_quad_data_package : in std_logic_vector (31 downto 0);
    	mvt_quad_data_package_valid : in std_logic;
    	mvt_quad_data_package_last : in std_logic;

		-- User ports ends
		-- Do not modify the ports beyond this line

		-- Global ports
		M_AXIS_ACLK	: in std_logic;
		-- 
		M_AXIS_ARESETN	: in std_logic;
		-- Master Stream Ports. TVALID indicates that the master is driving a valid transfer, A transfer takes place when both TVALID and TREADY are asserted. 
		M_AXIS_TVALID	: out std_logic;
		-- TDATA is the primary payload that is used to provide the data that is passing across the interface from the master.
		M_AXIS_TDATA	: out std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);
		-- TSTRB is the byte qualifier that indicates whether the content of the associated byte of TDATA is processed as a data byte or a position byte.
		M_AXIS_TSTRB	: out std_logic_vector((C_M_AXIS_TDATA_WIDTH/8)-1 downto 0);
		-- TLAST indicates the boundary of a packet.
		M_AXIS_TLAST	: out std_logic;
		-- TREADY indicates that the slave can accept a transfer in the current cycle.
		M_AXIS_TREADY	: in std_logic
	);
end mvt_axi_stream;

architecture Behavioral of mvt_axi_stream is
                                 
	                                                                                  
	-- Define the states of state machine                                             
	-- The control state machine oversees the writing of input streaming data to the FIFO,
	-- and outputs the streaming data from the FIFO                                   
	type state is ( IDLE,        -- This is the initial/idle state                    
	                INIT_COUNTER,  -- This state initializes the counter, once        
	                                -- the counter reaches C_M_START_COUNT count,     
	                                -- the state machine changes state to SEND_STREAM  
	                SEND_STREAM);  -- In this state the                               
	                             -- stream data is output through M_AXIS_TDATA        

	-- AXI Stream internal signals
	--wait counter. The master waits for the user defined number of clock cycles before initiating a transfer.
--	signal count	: std_logic_vector(WAIT_COUNT_BITS-1 downto 0);
	--streaming data valid

	--FIFO implementation signals
	signal axis_data_out	: std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);
	signal axis_last        : std_logic;
	signal axis_valid       : std_logic;
	

begin
	-- I/O Connections assignments

	M_AXIS_TVALID	<= axis_valid;
	M_AXIS_TDATA	<= axis_data_out;
	M_AXIS_TLAST	<= axis_last;
	M_AXIS_TSTRB	<= (others => '1');
                                                                        
	  process(M_AXIS_ACLK)                                                          
	  variable  sig_one : integer := 1;                                             
	  begin                                                                         
	    if (rising_edge (M_AXIS_ACLK)) then                                         
	      if(M_AXIS_ARESETN = '0') then                                             
	    	axis_data_out <= std_logic_vector(to_unsigned(sig_one,C_M_AXIS_TDATA_WIDTH));  
	    	axis_last <= '0';
	    	axis_valid <= '0';
	      else  
            axis_data_out <= mvt_quad_data_package;
            axis_last <= mvt_quad_data_package_last;
            axis_valid <= mvt_quad_data_package_valid;
	      end if;
	     end if;                                                                    
	   end process;                                                                 

	-- Add user logic here


	-- User logic ends

end Behavioral;




