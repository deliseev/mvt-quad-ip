----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eliseev
-- 
-- Create Date: 02/26/2023 01:51:07 PM
-- Design Name: 
-- Module Name: mvt_simulator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mvt_simulator is
--  Port ( );
end mvt_simulator;

architecture Behavioral of mvt_simulator is

component mvt_input_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 5
	);
	port (
		-- Users to add ports here
        mvt_in_p: in std_logic_vector (3 downto 0);
        mvt_in_n: in std_logic_vector (3 downto 0);   
        mvt_out: out std_logic_vector (3 downto 0);  
        
        test_in : in std_logic_vector (3 downto 0);  
        sync_in : in std_logic;
        clk_in : in std_logic;
        aresetn : in std_logic;
        dbg_counter : out std_logic_vector (15 downto 0);
 
        mvt0_rising_edge_counts_dbg : out std_logic_vector (15 downto 0);
        mvt0_falling_edge_counts_dbg : out std_logic_vector (15 downto 0);

        mvt1_rising_edge_counts_dbg : out std_logic_vector (15 downto 0);
        mvt1_falling_edge_counts_dbg : out std_logic_vector (15 downto 0);

        mvt2_rising_edge_counts_dbg : out std_logic_vector (15 downto 0);
        mvt2_falling_edge_counts_dbg : out std_logic_vector (15 downto 0);

        mvt3_rising_edge_counts_dbg : out std_logic_vector (15 downto 0);
        mvt3_falling_edge_counts_dbg : out std_logic_vector (15 downto 0);

		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
	    s00_axi_rready	: in std_logic;
	    
	    -- Ports of Axi Stream Master S00_AXIS	
	    s00_axis_tready: in std_logic;
	    s00_axis_tdata: out std_logic_vector(31 downto 0);
	    s00_axis_tlast: out std_logic;
	    s00_axis_tvalid: out std_logic
		
	);
end component;



  signal sync_in        : std_logic := '0';
  signal clk_in         : std_logic := '0';
  signal aresetn        : std_logic := '0';

  signal s00_axis_tready : std_logic := '0';
  signal s00_axis_tdata  : std_logic_vector(31 downto 0) := (others => '0');
  signal s00_axis_tlast  : std_logic := '0';
  signal s00_axis_tvalid : std_logic := '0';

begin

  dut_mvt_input_v1_0 : entity work.mvt_input_v1_0
  port map (
    mvt_in_p => "0000",
    mvt_in_n => "0000",
    mvt_out => open,  
    test_in => "0000",
    sync_in        => sync_in,
    clk_in         => clk_in,
    aresetn        => aresetn,
    dbg_counter  => open,
    
    mvt0_rising_edge_counts_dbg => open,
    mvt0_falling_edge_counts_dbg=> open,
    mvt1_rising_edge_counts_dbg=> open, 
    mvt1_falling_edge_counts_dbg=> open,
    mvt2_rising_edge_counts_dbg => open,
    mvt2_falling_edge_counts_dbg => open,
    mvt3_rising_edge_counts_dbg => open,
    mvt3_falling_edge_counts_dbg=> open, 
    
    s00_axi_awaddr	=> (others =>'0'),
	s00_axi_awprot	=> (others =>'0'),
	s00_axi_awvalid	=> '0', 
	s00_axi_awready	=> open,
	s00_axi_wdata	=> (others =>'0'),
	s00_axi_wstrb	=> (others =>'0'),
	s00_axi_wvalid => '0', 
	s00_axi_wready	=> open,
	s00_axi_bresp   => open,
	s00_axi_bvalid	=> open,
	s00_axi_bready	=> '0',
	s00_axi_araddr	=> (others =>'0'),
	s00_axi_arprot	=> (others =>'0'),
	s00_axi_arvalid => '0',
	s00_axi_arready	=> open,
	s00_axi_rdata	=> open,
	s00_axi_rresp	=> open,
    s00_axi_rvalid  => open,
	s00_axi_rready	=> '0',    
    s00_axis_tready => s00_axis_tready,
    s00_axis_tdata  => s00_axis_tdata,
    s00_axis_tlast  => s00_axis_tlast,
    s00_axis_tvalid => s00_axis_tvalid
  );

  -- Clock generation process
  clk_gen : process
  begin

      clk_in <= not clk_in;
      wait for 5 ns;
  end process clk_gen;

  -- Stimulus process
  reset_proc : process
  begin
    aresetn <= '0';
    wait for 50 ns;
    aresetn <= '1';
    wait;
  end process reset_proc;


  -- sync signal
  stim_proc : process
  begin
    sync_in <= '0';
    wait for 100 ns;
    sync_in <= '1';
    wait for 100 ns;
    sync_in <= '0';
    wait for 800 ns;
    sync_in <= '1';
    wait for 90 ns;
    sync_in <= '0';
    wait for 270 ns;
    sync_in <= '1';
    wait for 90 ns;
    sync_in <= '0';
    wait for 360 ns;
    sync_in <= '1';
    wait for 50 ns;
    sync_in <= '0';
    wait for 100us;
  end process stim_proc;

  axi_process : process
  begin
    s00_axis_tready <= '0';
    wait for 80 ns;
    s00_axis_tready <= '1';
    wait for 30 ns;

    s00_axis_tready <= '0';
    wait for 30 ns;
    
    s00_axis_tready <= '1';
    wait for 100 ns;
    
    s00_axis_tready <= '0';
    wait for 20 ns;
    s00_axis_tready <= '1';
    wait for 280 ns;
    
    s00_axis_tready <= '0';
    wait for 100 ns;
    s00_axis_tready <= '1';
    wait for 150 ns;
    
    s00_axis_tready <= '0';
    wait for 100 us;
    
  end process axi_process;

end architecture;