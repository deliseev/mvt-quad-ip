----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/16/2023 03:42:06 PM
-- Design Name: 
-- Module Name: data_packager - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

Library UNISIM;
library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use UNISIM.vcomponents.all;

entity data_packager is
    Port ( main_counter : in STD_LOGIC_VECTOR (31 downto 0);
           rst_in : in STD_LOGIC;
           system_clk : in STD_LOGIC;
           data_package_32bit : out STD_LOGIC_VECTOR (31 downto 0);
           data_package_valid : out std_logic;
           data_package_last : out std_logic;
           mvt1_posedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt1_negedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt2_posedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt2_negedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt3_posedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt3_negedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt4_posedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           mvt4_negedge_index : in STD_LOGIC_VECTOR (3 downto 0);
           posedges_validity : in STD_LOGIC_VECTOR (3 downto 0);
           negedges_validity : in STD_LOGIC_VECTOR (3 downto 0)
           );
end data_packager;

architecture Behavioral of data_packager is

signal mvt1_byte : std_logic_vector (7 downto 0);
signal mvt2_byte : std_logic_vector (7 downto 0);
signal mvt3_byte : std_logic_vector (7 downto 0);
signal mvt4_byte : std_logic_vector (7 downto 0);

type State_Type is (RESET, IDLE, PAYLOAD_TRANSFER);
signal Current_State, Next_State : State_Type;
signal mvt_payload              : STD_LOGIC_VECTOR (31 downto 0);


begin

    mvt1_byte <= posedges_validity(0) & mvt1_posedge_index(2 downto 0) & negedges_validity(0) & mvt1_negedge_index(2 downto 0);
    mvt2_byte <= posedges_validity(1) & mvt2_posedge_index(2 downto 0) & negedges_validity(1) & mvt2_negedge_index(2 downto 0);
    mvt3_byte <= posedges_validity(2) & mvt3_posedge_index(2 downto 0) & negedges_validity(2) & mvt3_negedge_index(2 downto 0);
    mvt4_byte <= posedges_validity(3) & mvt4_posedge_index(2 downto 0) & negedges_validity(3) & mvt4_negedge_index(2 downto 0);
    
 
            
    process (system_clk, rst_in)
    begin
        if rst_in = '1' then
            -- Reset the state machine
            Current_State <= RESET;
        elsif rising_edge(system_clk) then
            -- State transition logic
            Current_State <= Next_State;
            
            mvt_payload <= mvt4_byte & mvt3_byte & mvt2_byte & mvt1_byte;
        end if;
    end process;
    

    SYNCHRO_OUTPUT_DECODE: process (system_clk, Current_State, posedges_validity, negedges_validity)
    begin
        if rising_edge(system_clk) then
            if Current_State = RESET then
                data_package_valid <= '0';
                data_package_last <= '0';
                data_package_32bit <= x"77777777";  
            elsif Current_State = IDLE then
                data_package_last <= '0';
                --if posedges_validity /= "0000" or negedges_validity /= "0000" then
                if not (posedges_validity="0000" and  negedges_validity="0000") then
                    data_package_valid <= '1';
                    data_package_32bit <= main_counter; -- transfer main counter state
                else
                    data_package_valid <= '0';
                    data_package_32bit <= x"99999999";
                end if;
    
            elsif Current_State = PAYLOAD_TRANSFER then
                data_package_valid <= '1';
                data_package_32bit <= mvt_payload;
                if (posedges_validity = "0000" and negedges_validity = "0000") then
                    data_package_last <= '1';  
                else 
                    data_package_last <= '0';
                end if;
            else 
                data_package_valid <= '0';
                data_package_last <= '0';
                data_package_32bit <= x"CAFECAFE";  
            end if;
        end if;
    end process;

    NEXTSTATE_DECODE: process (Current_State, main_counter, posedges_validity, negedges_validity)
    begin
        case Current_State is
            when RESET =>
                -- Initialize values after reset
                Next_State <= IDLE;
            when IDLE =>
                if (posedges_validity/="0000") or (negedges_validity/="0000") then
                    -- Either posedges_validity or negedges_validity is non-zero
                    Next_State <= PAYLOAD_TRANSFER;
                else 
                    Next_State <= IDLE;
                end if;
            when PAYLOAD_TRANSFER =>
                -- Payload transfer cycle
                if (posedges_validity /= "0000" or negedges_validity /= "0000") then
                    -- Either posedges_validity or negedges_validity is non-zero
                    Next_State <= PAYLOAD_TRANSFER;
                else
                    -- Both validity vectors are zero
                    Next_State <= IDLE;
                end if;

            when others =>
                Next_State <= RESET;
        end case;
    end process;

end Behavioral;
