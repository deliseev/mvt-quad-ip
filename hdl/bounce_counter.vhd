----------------------------------------------------------------------------------
-- Company: RWTH Aachen University
-- Engineer: Dmitry Eliseev
-- 
-- Create Date: 12/13/2022 09:50:48 PM
-- Design Name: 
-- Module Name: counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bounce_counter is
    Port ( bounce_in : in STD_LOGIC;
           reset : in STD_LOGIC;
           clk : in STD_LOGIC;
           flank_counter : out STD_LOGIC_VECTOR (31 downto 0));
end bounce_counter;

architecture Behavioral of bounce_counter is

signal flank_counter_reg : unsigned (31 downto 0);
signal posedge_detect : STD_LOGIC_VECTOR (1 downto 0) := "00";

begin

    process (clk)
    begin
        if (rising_edge (clk)) then
            posedge_detect(1)<=posedge_detect(0);
            posedge_detect(0)<=bounce_in;
        end if;
    end process;
    
    
    process (clk, reset)
    begin
    if (rising_edge (clk)) then
	    if ( reset = '1' ) then
	      flank_counter_reg  <= (others => '0');
	    else
	      if (posedge_detect = "01") then
	          flank_counter_reg <= flank_counter_reg+1;
	      end if;   
	    end if;
	  end if;
    end process;
    

    flank_counter<= std_logic_vector(flank_counter_reg);
    
end Behavioral;
