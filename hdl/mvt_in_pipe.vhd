----------------------------------------------------------------------------------
-- Company:  RWTH Aachen University
-- Engineer: Dmitry Eliseev
-- 
-- Create Date: 06/01/2023 10:35:18 PM
-- Design Name: 
-- Module Name: mvt_in_pipe - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 1.0 - Added posedge finder
-- Revision 0.5 - make FIFO output working synchronously to system_clk
-- Revision 0.1 - Added deserializer
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity frontend_edge_finder is
    Port ( in_p : in STD_LOGIC;
           in_n : in STD_LOGIC;
           clk_fast_00 : in STD_LOGIC;
           clk_fast_180 : in STD_LOGIC;
           clk_div : in STD_LOGIC;
           system_clk : in STD_LOGIC;
           rst_in : in STD_LOGIC;
           polarity : in STD_LOGIC;
           sample_out : out STD_LOGIC_VECTOR (7 downto 0);
           posedge_index : out std_logic_vector(3 downto 0);
           posedge_index_valid : out std_logic;
           negedge_index : out std_logic_vector(3 downto 0);
           negedge_index_valid : out std_logic
           );
end frontend_edge_finder;

architecture Behavioral of frontend_edge_finder is

signal serial_se_from_comparator : std_logic;
signal deser_out : std_logic_vector (7 downto 0);
signal n_deser_out : std_logic_vector (7 downto 0);

signal deser_polarity_aligned : std_logic_vector (7 downto 0);
signal deser_prev : std_logic_vector (7 downto 0);
signal concat_9bit_deser_sample : std_logic_vector (8 downto 0);

signal index_pos_edge : std_logic_vector(3 downto 0);  
signal posedge_found : std_logic;

signal index_neg_edge : std_logic_vector(3 downto 0);  
signal negedge_found : std_logic;
  


begin

   IBUFDS_inst : IBUFDS
   generic map (
      DIFF_TERM => FALSE, -- Differential Termination 
      IBUF_LOW_PWR => TRUE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD => "DEFAULT")
   port map (
      O => serial_se_from_comparator,  -- Buffer output
      I => in_p,      -- Diff_p buffer input (connect directly to top-level port)
      IB => in_n      -- Diff_n buffer input (connect directly to top-level port)
   );

   input_deserializer: ISERDESE3
   generic map (
      DATA_WIDTH     => 8,
      FIFO_ENABLE    => "TRUE",
      FIFO_SYNC_MODE => "FALSE",
      SIM_DEVICE     => "ULTRASCALE_PLUS")
   port map (
      D           => serial_se_from_comparator,
      Q           => deser_out,
      CLK         => clk_fast_00,
      CLK_B       => clk_fast_180,
      CLKDIV      => clk_div,
      RST         => rst_in,
      FIFO_RD_CLK => system_clk,
      FIFO_RD_EN  => '1',
      FIFO_EMPTY  => open);
      
    n_deser_out  <= not deser_out;
    deser_polarity_aligned <= deser_out when (polarity = '1') else n_deser_out;
    sample_out <= deser_polarity_aligned;
    
    -- The deserialized bit vector from the previous clock tact and the first bit
    -- of the most recent deserialized bit vector are glued together. This results in a 9-bit vector.
    concat_9bit_deser_sample <=deser_polarity_aligned (0) & deser_prev;    
    
    -- Here is a priority encoder to find the positive transition in the incoming serial bitstream,
    -- The temporaly most recent transmitted serial bit becomes the MSB in the deserialized bit-vector.
    -- This means we need to find the occurance of the combination '10' in this bit-vector.  
    -- Result is loaded in the index_pos_edge register.
    pos_edge_finder: process(concat_9bit_deser_sample)
    begin

          index_pos_edge<="0000";
          posedge_found <= '0';
          
          -- Iterate through the input vector to find the occurance of '10'
          for i in 1 to 8 loop
            if concat_9bit_deser_sample ( i downto i-1) = "10" then
              -- Laden des Indexes des gesetzten Bits in das Ausgaberegister
                index_pos_edge <= std_logic_vector(to_unsigned(i, 4)-1); -- we need to sutbtract -1 in order to come to 0..7 band instead of 1..8 band 
                posedge_found <='1';  
            end if;
          end loop;
    end process;
    
    -- Here is a priority encoder to find the negative transition in the incoming serial bitstream,
    -- The earliest transmitted serial bit becomes the LSB in the deserialized bit-vector.
    -- This means we need to find the occurance of the combination '01' in the deserialized sample.  
    neg_edge_finder: process(concat_9bit_deser_sample)
    begin

          index_neg_edge<="0000";
          negedge_found <= '0';
          
          -- Iterate through the input vector to find the first bit that is set
          for i in 1 to 8 loop
            if concat_9bit_deser_sample ( i downto i-1) = "01" then
              -- Laden des Indexes des gesetzten Bits in das Ausgaberegister
                index_neg_edge <= std_logic_vector(to_unsigned(i, 4)-1); 
                negedge_found <='1';  
            end if;
          end loop;
    end process;
    
    
    clk_routine: process(system_clk)
    begin
        if rising_edge(system_clk) then
            posedge_index   <= index_pos_edge;
            negedge_index   <= index_neg_edge;
            deser_prev      <= deser_polarity_aligned;
            posedge_index_valid <= posedge_found;
            negedge_index_valid <= negedge_found;
        end if;
    end process;

end Behavioral;


