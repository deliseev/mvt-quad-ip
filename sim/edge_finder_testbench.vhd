library IEEE;
use IEEE.std_logic_1164.all;

entity edge_finder_tb is
end edge_finder_tb;




architecture Behavioral of edge_finder_tb is

  -- Instantiate the mvt_in_pipe unit
  component frontend_edge_finder
    Port (
      in_p          : in  std_logic;
      in_n          : in  std_logic;
      clk_fast_00   : in  std_logic;
      clk_fast_180  : in  std_logic;
      clk_div       : in  std_logic;
      system_clk    : in  std_logic;
      rst_in        : in  std_logic;
      polarity      : in  std_logic;
      sample_out    : out std_logic_vector(7 downto 0);
      posedge_index : out std_logic_vector(3 downto 0);
      posedge_index_valid : out std_logic ;
      negedge_index : out std_logic_vector(3 downto 0);
      negedge_index_valid : out std_logic 
    );
  end component;

  -- Constants declaration
  constant CLK_FAST_PERIOD : time := 2.5 ns;      -- 400 MHz clock period (2.5 ns)
  constant DATA_BIT_PERIOD : time := 1.25 ns;     -- Data bit period (1.25 ns)
  constant SIMUL_TIME : time := 800 ns;



  -- Signal declarations
  signal data_signal_tb   : std_logic;
  signal clk_fast_00_tb   : std_logic := '0';
  signal clk_fast_180_tb  : std_logic := '1';
  signal clk_div_tb       : std_logic := '1';
  signal system_clk_tb    : std_logic := '1';
  signal rst_in_tb        : std_logic := '0';
  signal polarity_tb      : std_logic := '1';
  signal sample_out_tb    : std_logic_vector(7 downto 0);
  signal posedge_index_tb : std_logic_vector(3 downto 0);
  signal in_p_tb : std_logic;
  signal in_n_tb : std_logic;
  signal posedge_index_valid : std_logic;
  
begin

  -- Instantiate the mvt_in_pipe entity
  dut: frontend_edge_finder
    port map (
      in_p   => in_p_tb,
      in_n => in_n_tb,
      clk_fast_00   => clk_fast_00_tb,
      clk_fast_180  => clk_fast_180_tb,
      clk_div       => clk_div_tb,
      system_clk    => system_clk_tb,
      rst_in        => rst_in_tb,
      polarity      => polarity_tb,
      sample_out    => sample_out_tb,
      posedge_index => posedge_index_tb,
      posedge_index_valid => posedge_index_valid
    );

  in_p_tb <= '1' when (data_signal_tb ='1') else '0';
  in_n_tb <= '0' when (data_signal_tb ='1') else '1';
  
  -- Clock generation processes
  clk_fast_00_proc: process
  begin
    while now < SIMUL_TIME  loop  -- Run for SIMUL_TIME ns
      clk_fast_00_tb <= not clk_fast_00_tb;
      wait for CLK_FAST_PERIOD / 2;  -- Toggle every half period
    end loop;
    wait;
  end process;

  clk_fast_180_proc: process
  begin
    while now < SIMUL_TIME  loop  -- Run for SIMUL_TIME ns
      clk_fast_180_tb <= not clk_fast_180_tb;
      wait for CLK_FAST_PERIOD / 2;  -- Toggle every half period
    end loop;
    wait;
  end process;

  clk_div_proc: process
  begin
    while now < SIMUL_TIME  loop  -- Run for SIMUL_TIME ns
      clk_div_tb <= not clk_div_tb;
      wait for 2*CLK_FAST_PERIOD;  
    end loop;
    wait;
  end process;

  system_clk_proc: process
  begin
    wait for 1.3 ns;
    while now < SIMUL_TIME  loop  -- Run for SIMUL_TIME ns
      system_clk_tb <= not clk_div_tb;
      wait for 2*CLK_FAST_PERIOD; 
    end loop;
    wait;
  end process;
  


  -- Data signal generation process
  data_signal_proc: process
  begin
    wait for 2*CLK_FAST_PERIOD; 

    while now < SIMUL_TIME  loop
        for i in 0 to 20 loop
          case i is
            when 0 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
            when 1 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
            when 2 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
            when 3 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';    
            when 4 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';                     
            when 5 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';    
            when 6 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';   
            when 7 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';   
            when 8 =>
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';   
             when 9 =>
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';   
             when 10 =>
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';     
             when 11 =>
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
 
            when others =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
          end case;
          wait for DATA_BIT_PERIOD;
        end loop;
    end loop;
end process;

end architecture;
