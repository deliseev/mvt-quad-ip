library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
entity AXI_Master_Interface is
    Port (
        -- AXI Master Signals
        axi_aclk   : in  std_logic;
        axi_awaddr : out std_logic_vector(4 downto 0);
        axi_awprot : out std_logic_vector(2 downto 0);
        axi_awvalid: out std_logic;
        axi_awready: in  std_logic;  -- AXI Slave's readiness for address
        axi_wdata  : out std_logic_vector(31 downto 0);
        axi_wstrb  : out std_logic_vector(3 downto 0);
        axi_wvalid : out std_logic;
        axi_wready : in  std_logic;  -- AXI Slave's readiness for data
        axi_bvalid : in  std_logic;  -- AXI Slave's response valid
        axi_bready : out std_logic; -- AXI Slave's readiness for response
        axi_araddr : out std_logic_vector(4 downto 0);
        axi_arprot : out std_logic_vector(2 downto 0);
        axi_arvalid: out std_logic;
        axi_arready: in  std_logic;  -- AXI Slave's readiness for read address
        axi_rready : in  std_logic;  -- AXI Slave's readiness for read data
        -- Additional input signal
        polarity   : in  std_logic_vector(3 downto 0)
    );
end entity AXI_Master_Interface;
architecture Behavioral of AXI_Master_Interface is
    type state_type is (IDLE, WRITE_REQUEST, WRITE_WAIT, READ_REQUEST, READ_WAIT);
    signal current_state : state_type := IDLE;
begin
    process(axi_aclk)
    begin
        if rising_edge(axi_aclk) then
            case current_state is
                when IDLE =>
                    axi_awaddr <= "00100"; -- 5-bit address: b"001"
                    axi_awprot <= "000";
                    axi_awvalid <= '1';
                    axi_wdata <= x"0000000" & polarity;
                    axi_wstrb <= "1111";
                    axi_wvalid <= '1';
                    axi_bready <= '1'; -- Master asserts BREADY for write
                    current_state <= WRITE_REQUEST;


                when WRITE_REQUEST =>
                    if axi_awready = '1' and axi_wready = '1' then
                        axi_awvalid <= '0';
                        axi_wvalid <= '0';
                        axi_bready <= '0'; -- Deassert BREADY
                        current_state <= WRITE_WAIT;
                    end if;

                when WRITE_WAIT =>
                    if axi_bvalid = '1' then
                        -- Wait for the Slave to assert BVALID, indicating response received
                        current_state <= READ_REQUEST;
                    end if;

                when READ_REQUEST =>
                    axi_araddr <= "00100"; -- 5-bit address: b"001"
                    axi_arprot <= "000";
                    axi_arvalid <= '1';
                    current_state <= READ_WAIT;

                when READ_WAIT =>
                    if axi_rready = '1' then
                        -- Wait for the Slave to assert RREADY, indicating read data available
                        -- You can handle read data here (optional, depends on the application)
                        current_state <= IDLE;
                    end if;
                    
                when others =>
                    current_state <= IDLE;
            end case;
        end if;
    end process;
end architecture Behavioral;
