library IEEE;
use IEEE.std_logic_1164.all;

entity top_testbench is
end top_testbench;




architecture Behavioral of top_testbench is

    component mvt_quad is
--	generic (
--		-- Users to add parameters here

--		-- User parameters ends
--		-- Do not modify the parameters beyond this line
--		-- Parameters of Axi Slave Bus Interface S00_AXI
--		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
--		C_S00_AXI_ADDR_WIDTH	: integer	:= 5
--	);
	port (
		-- Users to add ports here
        mvt_in_p: in std_logic_vector (3 downto 0);
        mvt_in_n: in std_logic_vector (3 downto 0);   
        
        sync_in : in std_logic;
        clk_in : in std_logic;
        aresetn : in std_logic;
        
        clk_fast_00  : in std_logic; -- fast clock for deserializers
        clk_fast_180 : in std_logic; -- inverted  fast clock deserializers
        clk_div_00  : in std_logic;
        
        reset_main_counter : in std_logic; -- reset for a 32-bit counter (system clock)
               
        -- debug signals:
        mvt_serdes_out_dbg_0 : out std_logic_vector (7 downto 0);
        mvt_serdes_out_dbg_1 : out std_logic_vector (7 downto 0);
        mvt_serdes_out_dbg_2 : out std_logic_vector (7 downto 0);
        mvt_serdes_out_dbg_3 : out std_logic_vector (7 downto 0);
        
        posedge_index_dbg_0 : out std_logic_vector(3 downto 0);
        posedge_index_dbg_1 : out std_logic_vector(3 downto 0);
        posedge_index_dbg_2 : out std_logic_vector(3 downto 0);
        posedge_index_dbg_3 : out std_logic_vector(3 downto 0);
        p_indices_valid_dbg : out std_logic_vector(3 downto 0);
        
        negedge_index_dbg_0 : out std_logic_vector(3 downto 0);
        negedge_index_dbg_1 : out std_logic_vector(3 downto 0);
        negedge_index_dbg_2 : out std_logic_vector(3 downto 0);
        negedge_index_dbg_3 : out std_logic_vector(3 downto 0);
        n_indices_valid_dbg : out std_logic_vector(3 downto 0);
        
        data_package_dbg : out std_logic_vector(63 downto 0);

		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_awaddr	: in std_logic_vector(4 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(31 downto 0);
		s00_axi_wstrb	: in std_logic_vector((32/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(4 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(31 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
	    s00_axi_rready	: in std_logic;
	    
	    -- Ports of Axi Stream Master S00_AXIS	
	    s00_axis_tready: in std_logic;
	    s00_axis_tdata: out std_logic_vector(31 downto 0);
	    s00_axis_tlast: out std_logic;
	    s00_axis_tvalid: out std_logic
		
	);
end component;

component AXI_Master_Interface is
  Port (
        -- AXI Master Signals
        axi_aclk   : in  std_logic;
        axi_awaddr : out std_logic_vector(4 downto 0);
        axi_awprot : out std_logic_vector(2 downto 0);
        axi_awvalid: out std_logic;
        axi_awready: in  std_logic;  -- AXI Slave's readiness for address
        axi_wdata  : out std_logic_vector(31 downto 0);
        axi_wstrb  : out std_logic_vector(3 downto 0);
        axi_wvalid : out std_logic;
        axi_wready : in  std_logic;  -- AXI Slave's readiness for data
        axi_bvalid : in  std_logic;  -- AXI Slave's response valid
        axi_bready : out std_logic;  -- AXI Slave's readiness for response
        axi_araddr : out std_logic_vector(4 downto 0);
        axi_arprot : out std_logic_vector(2 downto 0);
        axi_arvalid: out std_logic;
        axi_arready: in  std_logic;  -- AXI Slave's readiness for read address
        axi_rready : in  std_logic;  -- AXI Slave's readiness for read data
        -- Additional input signal
        polarity   : in  std_logic_vector(3 downto 0)
    );
end component;

  -- Constants declaration
  constant CLK_FAST_PERIOD : time := 2.5 ns;      -- 400 MHz clock period (2.5 ns)
  constant DATA_BIT_PERIOD : time := 1.25 ns;     -- Data bit period (1.25 ns)
  constant SIMUL_TIME : time := 800 ns;



  -- Signal declarations
  signal data_signal_tb   : std_logic;
  signal clk_fast_00_tb   : std_logic := '0';
  signal clk_fast_180_tb  : std_logic := '1';
  signal clk_div_tb       : std_logic := '1';
  signal system_clk_tb    : std_logic := '1';
  signal mvt_polarity_tb      : std_logic_vector (3 downto 0) := "1111";

  signal in_p_tb : std_logic;
  signal in_n_tb : std_logic;

  signal reset_mc : std_logic;
  signal async_ip_core_rstn : std_logic;
  
  signal axi_awaddr : std_logic_vector (4 downto 0);
  signal axi_awprot : std_logic_vector (2 downto 0);
  signal axi_awvalid : std_logic;
  signal axi_wdata : std_logic_vector (31 downto 0);
  signal axi_wstrb : std_logic_vector (3 downto 0);
  signal axi_wvalid : std_logic;
  signal axi_bready : std_logic;
  signal axi_araddr : std_logic_vector (4 downto 0);
  signal axi_arprot : std_logic_vector(2 downto 0);
  signal axi_arvalid : std_logic;
  signal axi_rready : std_logic;
  signal axi_arready : std_logic;
  signal axi_awready : std_logic;
  signal axi_wready : std_logic;
  signal axi_bvalid : std_logic;

  
begin
  -- instantiate testbench for axi master
  m_axi: AXI_Master_Interface 
    port map (
        -- AXI Master Signals
        axi_aclk    => system_clk_tb,
        axi_awaddr  => axi_awaddr,
        axi_awprot  => axi_awprot,
        axi_awvalid => axi_awvalid,
        axi_awready => axi_awready,
        axi_wdata   => axi_wdata,
        axi_wstrb   => axi_wstrb,
        axi_wvalid  => axi_wvalid,
        axi_wready  => axi_wready,
        axi_bvalid  => axi_bvalid,
        axi_bready  => axi_bready,
        axi_araddr  => axi_araddr,
        axi_arprot  => axi_arprot,
        axi_arvalid => axi_arvalid,
        axi_arready  => axi_arready,
        axi_rready  => axi_rready,
        -- Additional input signal
        polarity    => mvt_polarity_tb
    );


  -- Instantiate the mvt_in_pipe entity
  dut: mvt_quad
--    generic map (axilite_data_width,
--                 axilite_addr_width)
    port map (
      mvt_in_p   => (others => in_p_tb),
      mvt_in_n => (others => in_n_tb),
      sync_in => '0',
      clk_in => system_clk_tb,
      clk_fast_00   => clk_fast_00_tb,
      clk_fast_180  => clk_fast_180_tb,
      clk_div_00   => clk_div_tb,
      aresetn => async_ip_core_rstn,
      reset_main_counter => reset_mc,
      s00_axi_awaddr  => axi_awaddr,
	  s00_axi_awprot  => axi_awprot,
	  s00_axi_awvalid => axi_awvalid,
	  s00_axi_awready => axi_awready,
      s00_axi_wdata	  => axi_wdata,
	  s00_axi_wstrb	  => axi_wstrb,
	  s00_axi_wvalid =>  axi_wvalid,
      s00_axi_wready => axi_wready,
      s00_axi_bresp	 => open,
      s00_axi_bvalid => axi_bvalid,
	  s00_axi_bready => axi_bready,
	  s00_axi_araddr => axi_araddr,
	  s00_axi_arprot =>  axi_arprot,
	  s00_axi_arvalid => axi_arvalid,
	  s00_axi_rready => axi_rready,
      s00_axis_tready => '1'
	        
    );

  in_p_tb <= '1' when (data_signal_tb ='1') else '0';
  in_n_tb <= '0' when (data_signal_tb ='1') else '1';

  async_reset: process
  begin
    async_ip_core_rstn <= '0';
    wait for 47 ns;
    async_ip_core_rstn <= '1';
    wait;
  end process;
  
  mc_reset: process
  begin
    reset_mc <= '0';
    wait for 200 ns;
    reset_mc <= '1';
    wait for 10 ns;
    reset_mc <= '0';
    wait;
  end process;
  
  -- Clock generation processes
  clk_fast_00_proc: process
  begin
    while now < SIMUL_TIME  loop  -- Run for SIMUL_TIME ns
      clk_fast_00_tb <= not clk_fast_00_tb;
      wait for CLK_FAST_PERIOD / 2;  -- Toggle every half period
    end loop;
    wait;
  end process;

  clk_fast_180_proc: process
  begin
    while now < SIMUL_TIME  loop  -- Run for SIMUL_TIME ns
      clk_fast_180_tb <= not clk_fast_180_tb;
      wait for CLK_FAST_PERIOD / 2;  -- Toggle every half period
    end loop;
    wait;
  end process;

  clk_div_proc: process
  begin
    while now < SIMUL_TIME  loop  -- Run for SIMUL_TIME ns
      clk_div_tb <= not clk_div_tb;
      wait for 2*CLK_FAST_PERIOD;  
    end loop;
    wait;
  end process;

  system_clk_proc: process
  begin
    wait for 1.3 ns;
    while now < SIMUL_TIME  loop  -- Run for SIMUL_TIME ns
      system_clk_tb <= not clk_div_tb;
      wait for 2*CLK_FAST_PERIOD; 
    end loop;
    wait;
  end process;
  


  data_signal_proc: process
  begin
    wait for 2*CLK_FAST_PERIOD; 

    while now < SIMUL_TIME  loop
        for i in 0 to 20 loop
          case i is
            when 0 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
            when 1 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
            when 2 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
            when 3 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';    
            when 4 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';                     
            when 5 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';    
            when 6 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';   
            when 7 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '1';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';   
            when 8 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';   
             when 9 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';   
             when 10 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';     
             when 11 =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
 
            when others =>
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
              wait for DATA_BIT_PERIOD;
              data_signal_tb <= '0';
          end case;
          wait for DATA_BIT_PERIOD;
        end loop;
    end loop;
end process;


  -- Data signal generation process
--  data_signal_proc: process
--  begin
--    wait for 2*CLK_FAST_PERIOD; 

--    while now < SIMUL_TIME  loop
--        for i in 0 to 20 loop
--          case i is
--            when 0 =>
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--            when 1 =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--            when 2 =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--            when 3 =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';    
--            when 4 =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';                     
--            when 5 =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';    
--            when 6 =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';   
--            when 7 =>
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';   
--            when 8 =>
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';   
--             when 9 =>
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';   
--             when 10 =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';     
--             when 11 =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
 
--            when others =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--          end case;
--          wait for DATA_BIT_PERIOD;
--        end loop;
--    end loop;
--end process;


--  -- Data signal generation process
--  data_signal_proc: process
--  begin
--    wait for 2*CLK_FAST_PERIOD; 

--    while now < SIMUL_TIME  loop
--        for i in 0 to 20 loop
--          case i is
--            when 0 =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--            when 1 =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--            when 2 =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--            when 3 =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';    
--            when 4 =>
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--           when others =>
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '1';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';
--              wait for DATA_BIT_PERIOD;
--              data_signal_tb <= '0';    
--          end case;
--          wait for DATA_BIT_PERIOD;
--        end loop;
--    end loop;
--end process;

end architecture;
